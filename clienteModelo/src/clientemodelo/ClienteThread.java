/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clientemodelo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author glaubos
 */
public class ClienteThread implements Runnable{
     private Socket socket;

    public ClienteThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream(this.socket.getInputStream());
            Long inicio = (Long) inputStream.readObject();
            Long fim = (Long) inputStream.readObject();
            String log = "Recebido numeros de "+inicio+" a "+fim+" do mestre. \n";
            System.out.println(log);
            Long resultadoParcial = calcular(inicio, fim);
            log = "Enviado resultado da soma : "+ resultadoParcial+" para o mestre. \n";
            System.out.println(log);
            ObjectOutputStream outputStream = new ObjectOutputStream(this.socket.getOutputStream());
            outputStream.writeObject(resultadoParcial);
            inputStream.close();
            outputStream.close();
        } catch (ClassNotFoundException ex) {
           
        } catch (IOException ex) {
            
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {
                
            }
        }
    }
    
    private Long calcular(Long inicio, Long fim) {
        long resultadoParcial = 0;
        for (long i = inicio; i <= fim; i++) {
            resultadoParcial += i;
        }
        return resultadoParcial;
    }
     
     
}
