/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidormodelo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

/**
 *
 * @author glaubos
 */
public class ServidorThread implements Runnable {

    private ServerSocket servidor;
    private List<Socket> clientes;

    public ServerSocket getServer() {
        return servidor;
    }

    public void setServer(ServerSocket servidor) {
        this.servidor = servidor;
    }

    public List<Socket> getSlaves() {
        return clientes;
    }

    public void setSlaves(List<Socket> clientes) {
        this.clientes = clientes;
    }

    public ServidorThread(ServerSocket servidor, List<Socket> clientes) {
        this.servidor = servidor;
        this.clientes = clientes;
    }

    @Override
    public void run() {
        Socket clientSocket = null;
        while (true) {
            try {

                clientSocket = this.servidor.accept();
                clientSocket.setKeepAlive(true);
                this.clientes.add(clientSocket);
                String porta = "cliente na Porta: " + clientSocket.getPort();
                System.out.println(porta);

            } catch (IOException ex) {
            }
        }
    }
}
