/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidormodelo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author glaubos
 */
public class CalcularThread implements Runnable {

    private Socket client;
    private Long inicio;
    private Long fim;
    private Servidor master;   

    CalcularThread(Socket clientSocket, Long inicio, Long fim, Servidor aMaster) {
        this.client = clientSocket;
        this.inicio = inicio;
        this.fim = fim;
        this.master = aMaster;

    }

    @Override
    public void run() {
        ObjectOutputStream anObjectOutputStream = null;
        try {
            anObjectOutputStream = new ObjectOutputStream(this.client.getOutputStream());
            anObjectOutputStream.writeObject(inicio);
            anObjectOutputStream.writeObject(fim);
            ObjectInputStream anObjectStream = new ObjectInputStream(this.client.getInputStream());
            Long aParcel = (Long) anObjectStream.readObject();
            String aLog = "Escravo de Porta: " + this.client.getPort() + " retornou o valor: " + aParcel + "\n";
           System.out.println(aLog);
            master.addResult(aParcel);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CalcularThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CalcularThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                anObjectOutputStream.close();
            } catch (IOException ex) {
            }
        }
    }
}
